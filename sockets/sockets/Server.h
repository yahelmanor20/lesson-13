#pragma once
#include"Helper.h"
#include <WinSock2.h>
#include <Windows.h>
#include<thread>
#include<vector>
#include<string>
#include <mutex>
#include<fstream>
#include <chrono>
#include<queue>
#define SIZE 1024
//using namespace std;
class Server
{
public:
	Server();
	~Server();
	void serve(int port);

private:
	std::vector<std::string>users; // vector is more affective then a queue
	void accept();
	void clientHandler(SOCKET clientSocket);
	void sendAllClients(std::string CurrUserName);
	SOCKET _serverSocket;
	std::vector<SOCKET>allSockets;
	std::mutex lock;
	std::mutex fLock;
	std::ofstream file;//the shered file
};

