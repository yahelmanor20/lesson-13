#include "Server.h"
#include <exception>
#include <iostream>
#include <windows.h>

Server::Server()
{

	// this server use TCP. that why SOCK_STREAM & IPPROTO_TCP
	// if the server use UDP we will use: SOCK_DGRAM & IPPROTO_UDP
	_serverSocket = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);

	if (_serverSocket == INVALID_SOCKET)
		throw std::exception(__FUNCTION__ " - socket");
}

Server::~Server()
{
	try
	{
		// the only use of the destructor should be for freeing 
		// resources that was allocated in the constructor
		closesocket(_serverSocket);
	}
	catch (...) {}
}

void Server::serve(int port)
{

	struct sockaddr_in sa = { 0 };

	sa.sin_port = htons(port); // port that server will listen for
	sa.sin_family = AF_INET;   // must be AF_INET
	sa.sin_addr.s_addr = INADDR_ANY;    // when there are few ip's for the machine. We will use always "INADDR_ANY"

										// again stepping out to the global namespace
										// Connects between the socket and the configuration (port and etc..)
	if (bind(_serverSocket, (struct sockaddr*)&sa, sizeof(sa)) == SOCKET_ERROR)
		throw std::exception(__FUNCTION__ " - bind");

	// Start listening for incoming requests of clients
	if (listen(_serverSocket, SOMAXCONN) == SOCKET_ERROR)
		throw std::exception(__FUNCTION__ " - listen");
	std::cout << "Listening on port " << port << std::endl;

	while (true)
	{
		// the main thread is only accepting clients 
		// and add then to the list of handlers
		std::cout << "Waiting for client connection request" << std::endl;
		accept();
	}
}


void Server::accept()
{
	std::vector<std::thread*>threds;
	// notice that we step out to the global namespace
	// for the resolution of the function accept

	// this accepts the client and create a specific socket from server to this client
	SOCKET client_socket = ::accept(_serverSocket, NULL, NULL);

	if (client_socket == INVALID_SOCKET)
		throw std::exception(__FUNCTION__);

	std::cout << "Client accepted. Server and client can speak" << std::endl;
	allSockets.push_back(client_socket);
	// the function that handl v e the conversation with the client
	threds.push_back(new std::thread(&Server::clientHandler, this, client_socket));

	//clientHandler(client_socket);
	// there is bug in magshimim's system everytime that the getMessageTypeCode is called and there is no aout feedback the
	//thred is callsed abort() for few seconds and then countinue
	//so this is why the program is not on thread you can activate it but it will called abort
}


void Server::clientHandler(SOCKET clientSocket)
{
	std::unique_lock<std::mutex>locker(lock,std::defer_lock);//defer lock means that it not lock the mutex insantly but wait for lock()
	std::string name;
	std::queue<int> messeges;
	//messegeGetter(clientSocket, &messeges);
	int dataSize = 0,CurrUserSize = 0,typeCode=0,place = 1;
	std::string CurrUserName,data;
	try
	{
		
		while (typeCode != MT_CLIENT_EXIT)
		{
			typeCode = Helper::getMessageTypeCode(clientSocket);
			switch (typeCode)
			{
			case MT_CLIENT_LOG_IN:// log in case
			{
				CurrUserSize = Helper::getIntPartFromSocket(clientSocket, 2);
				CurrUserName = Helper::getStringPartFromSocket(clientSocket, CurrUserSize);

				std::cout << "Client name is: " << CurrUserName << std::endl;
				locker.lock();
				users.push_back(CurrUserName);
				locker.unlock();

				//send the 101 to all users
				sendAllClients(CurrUserName);
				break;
			}
			case MT_CLIENT_UPDATE: // the gui update every 10 seconds
				file.open("data.txt");
				dataSize = Helper::getIntPartFromSocket(clientSocket, 5);
				data = Helper::getStringPartFromSocket(clientSocket, dataSize);
				file << data;
				file.close();

				//send the 101 to all users
				sendAllClients(CurrUserName);
				break;
			case MT_CLIENT_FINISH: // when finish the client is going ti last of the queue and not get kicket out

				file.open("data.txt");
				dataSize = Helper::getIntPartFromSocket(clientSocket, 5);
				data = Helper::getStringPartFromSocket(clientSocket, dataSize);
				file << data;
				file.close();

				for (size_t i = 0; i < users.size(); i++) users[i] == CurrUserName ? place = i : 0;

				locker.lock();
				users.erase(users.begin() + place); // erase and push it again to the back(the lazy way:) )
				users.push_back(CurrUserName);
				locker.unlock();

				//send the 101 to all users
				sendAllClients(CurrUserName);
				break;
			case MT_CLIENT_EXIT:
				for (size_t i = 0; i < users.size(); i++) users[i] == CurrUserName ? place = i : 0;
				locker.lock();
				users.erase(users.begin() + place); // this time erase the user for real
				locker.unlock();
				for (size_t i = 0; i < allSockets.size(); i++) allSockets[i] == clientSocket ? place = i : 0;
				allSockets.erase(allSockets.begin() + place);
				if (allSockets.size())
				{
					sendAllClients(users.front());
				}
				break;
			default:
				break;
			}
			//Sleep(100);
		}
	}
	catch (const std::exception& e)
	{
		std::cout << e.what() << std::endl;
		closesocket(clientSocket);
	}
	closesocket(clientSocket);
}

void Server::sendAllClients(std::string CurrUserName)
{
	std::string data;
	std::fstream filefile;
	filefile.open("data.txt");
	int place = 1;//if the for not find anything the plcae will be 1

	char output[999];
	while (!filefile.eof())
	{
		filefile >> output;
		data.append(output);
		data.append(" ");
	}
	for (size_t i = 0; i < users.size(); i++) users[i] == CurrUserName ? place = i+1 : true; // finds the place of user
	for (size_t i = 0; i < allSockets.size();i++)//for each
	{
		Helper::sendUpdateMessageToClient(allSockets[i], data, users.front(), users.size() > 1 ? users[1] : "", place);
	}
	filefile.close();
}