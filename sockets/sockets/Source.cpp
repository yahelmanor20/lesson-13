#pragma comment (lib, "ws2_32.lib")

#include "WSAInitializer.h"
#include "Server.h"
#include <iostream>
#include <exception>

// there is bug in magshimim's system everytime that the getMessageTypeCode is called and there is no aout feedback the
//thred is callsed abort() for few seconds and then countinue
//so this is why the program is not on thread you can activate it but it will called abort
int main()
{
	std::cout << "starting!" << std::endl;
	try
	{
		WSAInitializer wsaInit;
		Server myServer;

		myServer.serve(8826);
	}
	catch (std::exception& e)
	{
		std::cout << "Error occured: " << e.what() << std::endl;
	}
	system("PAUSE");
	return 0;
}